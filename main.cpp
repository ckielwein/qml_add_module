#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlDebuggingEnabler>

int main(int argc, char* argv[])
{
	QQmlDebuggingEnabler enabler;
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;

	// Add Custom Module Import Path, as otherwise our custom modules are never loaded
	engine.addImportPath("qml");

    for (const auto& it : engine.importPathList())
		qDebug() << it;

	const QUrl url(QStringLiteral("qml/Main/main.qml"));

	QObject::connect(
			&engine,
			&QQmlApplicationEngine::objectCreated,
			&app,
			[url](QObject* obj, const QUrl& objUrl) {
				if (obj == nullptr && url == objUrl)
					QCoreApplication::exit(-1);
				},
				Qt::QueuedConnection);

	engine.load(url);

	return QGuiApplication::exec();
}
