# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "")
  file(REMOVE_RECURSE
  "CMakeFiles/qml_add_module_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/qml_add_module_autogen.dir/ParseCache.txt"
  "qml/Main/CMakeFiles/qml_main_autogen.dir/AutogenUsed.txt"
  "qml/Main/CMakeFiles/qml_main_autogen.dir/ParseCache.txt"
  "qml/Main/CMakeFiles/qml_mainplugin_autogen.dir/AutogenUsed.txt"
  "qml/Main/CMakeFiles/qml_mainplugin_autogen.dir/ParseCache.txt"
  "qml/Main/qml_main_autogen"
  "qml/Main/qml_mainplugin_autogen"
  "qml/Mymodule/CMakeFiles/qml_module_autogen.dir/AutogenUsed.txt"
  "qml/Mymodule/CMakeFiles/qml_module_autogen.dir/ParseCache.txt"
  "qml/Mymodule/CMakeFiles/qml_moduleplugin_autogen.dir/AutogenUsed.txt"
  "qml/Mymodule/CMakeFiles/qml_moduleplugin_autogen.dir/ParseCache.txt"
  "qml/Mymodule/qml_module_autogen"
  "qml/Mymodule/qml_moduleplugin_autogen"
  "qml_add_module_autogen"
  )
endif()
