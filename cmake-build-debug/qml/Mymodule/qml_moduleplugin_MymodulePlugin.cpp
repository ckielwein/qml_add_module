// This file is autogenerated by CMake. Do not edit.

#include <QtQml/qqmlextensionplugin.h>

extern void qml_register_types_Mymodule();
Q_GHS_KEEP_REFERENCE(qml_register_types_Mymodule);

class MymodulePlugin : public QQmlEngineExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlEngineExtensionInterface_iid)

public:
    MymodulePlugin(QObject *parent = nullptr) : QQmlEngineExtensionPlugin(parent)
    {
        volatile auto registration = &qml_register_types_Mymodule;
        Q_UNUSED(registration);
    }
};



#include "qml_moduleplugin_MymodulePlugin.moc"
